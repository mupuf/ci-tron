from unittest.mock import patch, MagicMock

from salad.console import SerialConsoleStream


@patch("salad.console.serial")
def test_SerialConsoleStream__recv(serial_mock):
    con = SerialConsoleStream("/dev/mydev")

    assert con.serial_dev == "/dev/mydev"
    assert con.device == serial_mock.Serial.return_value
    assert con._line_buffer == b""

    serial_mock.Serial.assert_called_with(con.serial_dev, baudrate=115200, timeout=0)
    device_mock = serial_mock.Serial.return_value

    # Test fileno()
    assert con.fileno() == device_mock.fileno.return_value

    # Test send()
    data = b"mydata"
    device_mock.write.assert_not_called()
    con.send(data)
    device_mock.write.assert_called_with(data)

    # Test recv()
    con.process_input_line = MagicMock()
    device_mock.read = MagicMock(side_effect=[b"hello world1\nhello world2"])
    con.recv()
    con.process_input_line.assert_called_once_with(b"hello world1")
    assert con._line_buffer == b"hello world2"

    # Test close()
    assert con.is_valid
    device_mock.close.assert_not_called()
    con.close()
    device_mock.close.assert_called_with()
    assert not con.is_valid
