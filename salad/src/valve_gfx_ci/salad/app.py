#!/usr/bin/env python3

from .tcpserver import SerialConsoleTCPServer
from .salad import salad
from waitress import serve

import socket
import flask
import json
import os


class CustomJSONEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, SerialConsoleTCPServer):
            return {
                "id": obj.id,
                "tcp_port": obj.port,
                "has_client": obj.client is not None,
                "tcp_port_logs": obj.server_for_logs.port,
            }

        return super().default(self, obj)


class CustomJSONProvider(flask.json.provider.JSONProvider):
    def dumps(self, obj, *args, **kwargs):
        return json.dumps(obj, cls=CustomJSONEncoder, *args, **kwargs)

    def loads(self, s, **kwargs):
        return json.loads(s)


app = flask.Flask(__name__)
app.json = CustomJSONProvider(app)


@app.route('/api/v1/machine', methods=['GET'])
def machines():
    return {
        "machines": dict([(m.id, m) for m in salad.machines]),
    }


@app.route('/api/v1/machine/<machine_id>', methods=['GET'])
def machine_id(machine_id):
    machine = salad.get_or_create_machine(machine_id)
    return CustomJSONEncoder().default(machine)


def socket_activated_sockets():
    def socket_from_fd(fd):
        return socket.socket(socket.AF_INET, socket.SOCK_STREAM, fileno=fd)

    # Ignore any FDs that were not meant for us
    if os.environ.get('LISTEN_PID', None) != str(os.getpid()):
        return []

    try:
        listen_fds_nr = int(os.getenv("LISTEN_FDS"))
    except Exception:
        return []

    FIRST_SOCKET_FD = 3
    return [socket_from_fd(FIRST_SOCKET_FD + i) for i in range(listen_fds_nr)]


def run():
    salad.start()
    if listen_sockets := socket_activated_sockets():
        serve(app, sockets=listen_sockets)
    else:
        serve(app, port=int(os.getenv("SALAD_PORT", "8005")))
    salad.stop()


if __name__ == '__main__':  # pragma: nocover
    run()
