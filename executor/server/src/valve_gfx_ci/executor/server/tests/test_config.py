from unittest.mock import patch
import server.config as config


@patch("server.config.socket.gethostname")
def test_get_farm_name_from_hostname(gethostname_mock):
    gethostname_mock.return_value = "unformated_name"
    assert config.get_farm_name_from_hostname() is None

    gethostname_mock.return_value = "testfarm-gateway"
    assert config.get_farm_name_from_hostname() == "testfarm"
