---
- name: Installing dependencies
  tags: install
  ansible.builtin.dnf5:
    install_weak_deps: false
    name:
      - gcc  # easysnmp needs gcc to compile
      - net-snmp-devel
      - python3
      - python3-pip

- name: Create the executor group
  become: true
  ansible.builtin.group:
    name: executor

- name: Create the executor user
  become: true
  ansible.builtin.user:
    name: executor
    group: executor
    groups:
      - minio
    shell: /bin/nologin

- name: Creates /app/ci-tron/executor/
  ansible.builtin.file:
    path: /app/ci-tron/executor/
    state: directory
    mode: '0755'

- name: Copy package
  ansible.posix.synchronize:
    src: "{{ executor_package_location }}"
    dest: /app/ci-tron/executor/
    rsync_opts:
      - "--exclude=.tox"
      - "--exclude=.venv"
      - "--exclude=htmlcov"
      - "--exclude=__pycache__"
      - "--exclude=*.pyc"
      - "--exclude=tmp/"
  register: executor_package

- name: Install executor  # noqa no-handler
  ansible.builtin.pip:
    name: "file:///app/ci-tron/executor/server"
  notify: 'Restart executor'
  # Always install when building the container, since the previous task won't
  # result in a "changed" status.
  # This is necessary because pip installing a local dist is never idempotent,
  # see: https://github.com/pypa/pip/pull/9147
  when: pid_1_name != "systemd" or executor_package.changed

- name: Installing ci-tron systemd.socket files
  become: true
  ansible.builtin.copy:
    src: "{{ item }}"
    dest: '/etc/systemd/system/'
    mode: '0644'
  with_items:
    - executor-dhcp.socket
    - executor-http.socket
    - executor-tftp.socket

- name: Install executor systemd unit file
  ansible.builtin.template:
    src: executor_systemd_unit.j2
    dest: /etc/systemd/system/executor.service
    owner: root
    group: root
    mode: '0644'
  notify: 'Restart executor'

- name: Enable executor service and dependencies
  become: true
  ansible.builtin.systemd:
    name: '{{ item }}'
    enabled: true
  with_items:
    - executor.service
    - executor-dhcp.socket
    - executor-http.socket
    - executor-tftp.socket

- name: configuring env
  become: true
  ansible.builtin.blockinfile:
    create: true
    mode: '0644'
    path: "{{ base_config_env_file }}"
    marker: "# {mark} ANSIBLE MANAGED BLOCK {{ role_name }}"
    block: |
      PRIVATE_INTERFACE=private
      CONSOLE_PATTERN_DEFAULT_MACHINE_UNFIT_FOR_SERVICE_REGEX="FATAL ERROR: The local machine doesn't match its expected state from MaRS$"
      EXECUTOR_ARTIFACT_CACHE_ROOT={{ cache_mount }}/executor/artifact_cache
      EXECUTOR_HTTP_IPv4_SOCKET_NAME=http_ipv4
      SERGENT_HARTMAN_BOOT_COUNT={{ sergent_hartman_boot_count }}
      SERGENT_HARTMAN_QUALIFYING_BOOT_COUNT={{ sergent_hartman_qualifying_boot_count }}
      SERGENT_HARTMAN_REGISTRATION_RETRIAL_DELAY={{ sergent_hartman_registration_retrial_delay }}
      GITLAB_CONF_FILE=/etc/gitlab-runner/config.toml
      BOOTS_DHCP_IPv4_SOCKET_NAME=dhcp_ipv4
      BOOTS_TFTP_IPv4_SOCKET_NAME=tftp_ipv4
      BOOTS_DEFAULT_KERNEL=http://{{ hostname }}:{{ minio_port }}/boot/default_kernel
      BOOTS_DEFAULT_INITRD=http://{{ hostname }}:{{ minio_port }}/boot/default_boot2container.cpio.xz
      BOOTS_DEFAULT_CMDLINE=b2c.container="-ti --tls-verify=false docker://{{ hostname }}:{{ fdo_proxy_registry_port }}/gfx-ci/ci-tron/machine-registration:latest register" b2c.ntp_peer="{{ hostname }}" b2c.cache_device=none loglevel=6 b2c.poweroff_delay=15 nomodeset console=tty0 console=ttynull
      MARS_DB_FILE=/config/mars_db.yaml
      # Variables with the "EXECUTOR_JOB__" prefix are shared by executor with
      # the job after removing the prefix and converting them to lower case

  notify: 'Restart executor'
