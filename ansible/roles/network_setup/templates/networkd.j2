#!/usr/bin/env python3

#
# Copyright © 2023 Valve Corporation
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice (including the next
# paragraph) shall be included in all copies or substantial portions of the
# Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
# IN THE SOFTWARE.

from enum import Enum
from functools import cached_property
from collections import defaultdict
import subprocess
import netifaces
import traceback
import argparse
import time
import sys
import os


class Network(Enum):
    PRIVATE = "private"
    PUBLIC = "public"
    MGMT = "mgmt"
    IGNORED = "ignored"

    @staticmethod
    def list():
        return list(map(lambda c: c.value, NicRole))


class NIC:
    @property
    def base_path(self):
        return os.path.join('/sys/class/net/', self.name)

    def __read_file(self, file_name):
        try:
            with open(os.path.join(self.base_path, file_name), 'r') as f:
                return f.read()
        except OSError:
            return ""

    def __init__(self, nic_name):
        self.name = nic_name

        if not os.path.isdir(self.base_path):
            raise ValueError('The NIC name does not exist')

    @property
    def ifindex(self):
        return int(self.__read_file('ifindex').strip())

    @property
    def mac_address(self):
        return self.__read_file('address').strip()

    @property
    def addresses(self):
        return netifaces.ifaddresses(self.name)

    @property
    def gateways(self):
        return netifaces.gateways(self.name)

    @property
    def is_connected(self):
        return not self.__read_file('carrier').strip() == '0'

    @property
    def is_bridge(self):
        return os.path.exists(os.path.join(self.base_path, 'bridge'))

    @property
    def is_tap_tun(self):
        return os.path.exists(os.path.join(self.base_path, 'tun_flags'))

    @property
    def is_wifi(self):
        return os.path.exists(os.path.join(self.base_path, 'phy80211'))

    @property
    def is_ethernet(self):
        return (self.__read_file('type').strip() == '1' and
                not self.is_bridge and
                not self.is_tap_tun and
                not self.is_wifi)

    @cached_property
    def network(self):
        # Look if the mac address is the static allocation list
        statically_allocated = False
        for network, static_list in static_allocs.items():
            if self.mac_address.lower() in static_list:
                return network

        # The MAC address was not statically-allocated, perform automatic allocation
        if self.is_ethernet:
            if len(self.addresses.get(netifaces.AF_INET, [])) > 0:
                return Network.PUBLIC
            else:
                return Network.PRIVATE
        elif self.is_wifi:
            return Network.MGMT

    def attach_to_bridge(self, bridge):
        if bridge not in ["private", "mgmt"]:
            print(f"Can't assign nic {self.name} to the bridge '{bridge}'")
            return

        subprocess.check_output(["ip", "link", "set", self.name, "down"])
        if self.is_wifi:
            cmdline = ["hostapd", "-i", self.name, "-B", f"{{ network_config }}/hostapd-{self.network.value }.conf"]

            # HACK: make sure no older instance of hostapd is running for this interface before running it again.
            # This fixes an hostapd error, given that the previous invocation was still somehow claiming the wifi nic
            subprocess.call(["pkill", "-f", " ".join(cmdline)])
            subprocess.check_output(cmdline)
        else:
            subprocess.check_output(["ip", "link", "set", self.name, "master", self.network.value])
        subprocess.check_output(["ip", "link", "set", self.name, "up"])

    def set_altname(self, altname):
        subprocess.call(["ip", "link", "property", "add", "dev", self.name, "altname", altname])

    def __repr__(self):
        return f"<NIC: {self.name}>"

    @staticmethod
    def list_ifs():
        nics = []
        for name in os.listdir('/sys/class/net/'):
            # Ignore all the podman-related interfaces
            if name.startswith("veth"):
                continue

            nics.append(NIC(name))
        return nics


def run():
    known_nics = dict()

    # HACK: Give a little bit of time to QEmu to bring up the interface
    time.sleep(1)

    while True:
        changed = False

        # Get the list of nics that we want to associate to different networks
        nics = [n for n in NIC.list_ifs() if n.is_ethernet or n.is_wifi]

        known_nic_names = {n for n in known_nics.keys()}
        nic_names = {n.name for n in nics}

        removed_nics_names = known_nic_names - nic_names
        if len(removed_nics_names) > 0:
            changed = True

            print(f"\nThe following nics got removed: {removed_nics_names}")
            for nic in removed_nics_names:
                known_nics.pop(nic)

        new_nics = [n for n in nics if n.name not in known_nics]
        if len(new_nics) > 0:
            changed = True

            print(f"\nFound the following new nics:")
            for nic in new_nics:
                print(f" * {nic.name}:")

                try:
                    if nic.network == Network.PUBLIC:
                        if not any([n.network == Network.PUBLIC for n in known_nics.values()]):
                            nic.set_altname("public")
                        else:
                            print(f"Ignoring the nic {nic.name} because we cannot have more than one public nic")
                            nic.network = Network.IGNORED
                    else:
                        nic.attach_to_bridge(nic.network.value)

                    print(f"   => Assigned to network {nic.network.name}")

                    known_nics[nic.name] = nic
                except Exception:
                    traceback.print_exc()

        if changed:
            # Notify systemd of the changes (it's safe to call it more than once)
            subprocess.call(["systemd-notify", "--ready"])

            # HACK: workaround systemd failing to detect that some interfaces just appeared
            subprocess.call(["systemctl", "daemon-reload"])

        # TODO: Switch to monitoring uevents
        time.sleep(1)


parser = argparse.ArgumentParser()
parser.add_argument("--ignore-interfaces", default="",
                    help=("Coma-separated list of MAC addresses of the interfaces that should not be added to any network. "
                          "Example: 'DE:AD:BE:EF:00:11, de:ad:be:ef:00:12,de:ad:be:ef:00:13'."))
parser.add_argument("--mgmt-static-alloc", default="",
                    help="Coma-separated list of MAC addresses of the interfaces that should be added to the mgmt network")
args = parser.parse_args()

static_allocs = dict()
for role, mac_list in [(Network.IGNORED, args.ignore_interfaces), (Network.MGMT, args.mgmt_static_alloc)]:
    if len(mac_list) > 0:
        static_allocs[role] = [f.lower().strip() for f in mac_list.split(',')]

run()
