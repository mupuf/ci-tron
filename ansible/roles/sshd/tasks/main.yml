---
- name: Installing ssh
  tags: install
  ansible.builtin.dnf5:
    name:
      - openssh-server
      - openssh-clients  # For scp/sftp to work
      - sssd-client
      - passwd           # For setup-insecure-ssh

- name: Mask the sshd-keygen@ecdsa services
  become: true
  ansible.builtin.systemd:
    name: sshd-keygen@ecdsa.service
    masked: true

- name: Mask the sshd-keygen@ed25519 services
  become: true
  ansible.builtin.systemd:
    name: sshd-keygen@ed25519.service
    masked: true

- name: Mask the sshd-keygen@rsa services
  become: true
  ansible.builtin.systemd:
    name: sshd-keygen@rsa.service
    masked: true

- name: Installing sshdgenkeys service configuration override
  become: true
  ansible.builtin.template:
    src: 'sshdgenkeys.service.j2'
    dest: '/etc/systemd/system/sshdgenkeys.service'
    mode: '0644'
    owner: root

- name: Enable sshdgenkeys service
  become: true
  ansible.builtin.systemd:
    name: sshdgenkeys.service
    enabled: true

- name: Ensure /etc/systemd/system/sshd.service.d/ directory exists
  become: true
  ansible.builtin.file:
    mode: '0755'
    path: /etc/systemd/system/sshd.service.d/
    state: directory

- name: Installing sshd service configuration override
  become: true
  ansible.builtin.template:
    src: 'sshd.service.d_override.conf.j2'
    dest: '/etc/systemd/system/sshd.service.d/override.conf'
    mode: '0644'
    owner: root
  notify: 'Restart sshd'

- name: Enable sshd service
  become: true
  ansible.builtin.systemd:
    name: sshd
    enabled: true

- name: Installing sshd_config
  become: true
  ansible.builtin.template:
    src: sshd_config.j2
    dest: "{{ default_sshd_config }}"
    owner: root
    group: root
    mode: '0644'
  notify: 'Restart sshd'

- name: Installing ssh nft rule
  become: true
  ansible.builtin.copy:
    src: 10-sshd.nft
    dest: /etc/nftables.d/10-sshd.nft
    owner: root
    group: root
    mode: '0644'
  notify: 'Restart nftables'

- name: Installing authorized keys
  become: true
  ansible.builtin.lineinfile:
    dest: "{{ default_ssh_authorized_keys }}"
    line: "{{ item }}"
    create: true
    mode: '0600'
    owner: root
    group: root
  with_items:
    - "# Default trusted users of ci-tron, feel free to overwrite!"
    - "{{ trusted_users | map(attribute='ssh.public_keys', default=[]) | flatten }}"

- name: Installing setup-insecure-ssh
  become: true
  ansible.builtin.template:
    src: setup-insecure-ssh.j2
    dest: /usr/local/bin/setup-insecure-ssh
    owner: root
    group: root
    mode: '0755'
  notify: 'Restart sshd'
