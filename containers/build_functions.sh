buildah_run="buildah run --isolation chroot"
buildah_commit="buildah commit --format docker"
build_container="no"

local_build() {
	if [ -n "${CI_REGISTRY:-}" ]; then
		return 1
    else
		return 0
	fi
}

push_image() {
	[ -n "$CI_JOB_TOKEN" ] && [ -n "$CI_REGISTRY" ] && podman login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
	extra_podman_args=
	[[ $IMAGE_NAME =~ ^localhost.* ]] && extra_podman_args='--tls-verify=false'

	if [ -z ${FDO_DISTRIBUTION_PLATFORMS+x} ]; then
		cmd="podman push $extra_podman_args $IMAGE_NAME"
	else
		cmd="podman manifest push $extra_podman_args $IMAGE_NAME $IMAGE_NAME"
	fi

	$cmd || true
	sleep 2
	$cmd
}

tag_latest_when_applicable() {
	# Pushing a new :latest tag should only happen when:
	# 1) when running locally (where $CI is unset)
	# 2) CI pipelines on the "default" branch of the project ($CI_COMMIT_BRANCH is set AND equal to CI_DEFAULT_BRANCH)
	tag_latest=false

	if local_build; then
		# running locally
		tag_latest=true
	elif [ -n "$CI_COMMIT_BRANCH" ] && [ "$CI_COMMIT_BRANCH" = "$CI_DEFAULT_BRANCH" ]; then
		# running on the default branch. CI_COMMIT_BRANCH is *not* set in MRs
		tag_latest=true
	fi

	if $tag_latest; then
		extra_skopeo_args=
		[[ $IMAGE_NAME =~ ^localhost.* ]] && extra_skopeo_args="$extra_skopeo_args --src-tls-verify=false"
		[[ $IMAGE_NAME_LATEST =~ ^localhost.* ]] && extra_skopeo_args="$extra_skopeo_args --dest-tls-verify=false"
		[ -n "$CI_JOB_TOKEN" ] && [ -n "$CI_REGISTRY" ] && skopeo login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
		skopeo copy --multi-arch all $extra_skopeo_args "docker://$IMAGE_NAME" "docker://$IMAGE_NAME_LATEST"
	fi
}

skopeo_inspect() {
	skopeomsg=`skopeo inspect --tls-verify=false docker://$IMAGE_NAME  2>&1 || true`

	if [[ $skopeomsg == *"manifest unknown"* ]]; then
		build_container="yes"
	elif [[ $skopeomsg == *"Digest"* ]] &&  [[ $skopeomsg == *"Layers"* ]]; then
		build_container="no"
	else
		build_container="network_problem"
	fi
}

check_if_build_needed_or_crash_on_network_error() {
	# when running locally (where $CI is unset)
	if local_build; then
		build_container="yes"
		return 0
	fi

	skopeo_inspect
	if [[ $build_container == "network_problem" ]]; then
		echo "There was a network problem, second try in 5 seconds..."
		sleep 5
		skopeo_inspect
		if [[ $build_container == "network_problem" ]]; then
			echo "The network problem is still present, third and last try in 10 seconds..."
			sleep 10
			skopeo_inspect
		fi
	fi

	if [[ $build_container == "network_problem" ]]; then
		# Fail the job, we don't know for sure whether the image is missing
		exit 1
	elif [[ $build_container == "yes" ]]; then
		return 0
	else
		return 1
	fi
}

# This function requires a "build()" function to be defined by the caller ahead of calling this function
build_and_push_single_arch_container() {
	echo "Building container ..."
	EXTRA_BUILDAH_FROM_ARGS='' build

	# Push it to the container registry
	if [ -n "$IMAGE_NAME" ]; then
		$buildah_commit $buildcntr $IMAGE_NAME
		push_image
	fi

	# Remove the local version of the container
	buildah unmount $buildcntr
	buildah rm $buildcntr
}

# This function requires a "build()" function to be defined by the caller ahead of calling this function
build_and_push_multi_arch_container() {
	local IMAGES=""
	for platform in ${FDO_DISTRIBUTION_PLATFORMS}; do
		echo -e "\n\nBuilding $platform container ..."
		EXTRA_BUILDAH_FROM_ARGS="--platform $platform" build

		ARCH_IMAGE_NAME=$IMAGE_NAME-${platform//\//-}
		$buildah_commit $buildcntr $ARCH_IMAGE_NAME
		IMAGES="$IMAGES $ARCH_IMAGE_NAME"

		buildah unmount $buildcntr
		buildah rm $buildcntr
	done

	# Create the manifest with all the images
	buildah manifest rm ${IMAGE_NAME} || /bin/true
	buildah manifest create ${IMAGE_NAME} ${IMAGES}

	# Push it to the container registry
	if [ -n "$IMAGE_NAME" ]; then
		push_image
	fi

	# Remove the temporary images
	podman rmi -f ${IMAGES}
}

# This function requires a "build()" function to be defined by the caller ahead of calling this function
build_and_push_container() {
	if check_if_build_needed_or_crash_on_network_error; then
		if [ -z ${FDO_DISTRIBUTION_PLATFORMS+x} ]; then
			build_and_push_single_arch_container
		else
			build_and_push_multi_arch_container
		fi
	fi

	# Make sure to tag the current container as latest on master
	if [ -n "$IMAGE_NAME_LATEST" ]; then
		tag_latest_when_applicable
	fi
}
