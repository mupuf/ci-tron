ci-tron_cleanup() {
    $buildah_run $buildcntr sh -c 'find /usr /etc /root -name __pycache__ -type d | xargs rm -rf'
	$buildah_run $buildcntr sh -c 'rm -f /usr/share/edk2/aarch64/{BL32_AP_MM.fd,QEMU_EFI-pflash.raw,QEMU_EFI-silent*,*.fd,vars-template-pflash.*} /usr/share/edk2/ovmf-4m/* /usr/share/edk2/ovmf/{*.efi,MICROVM.fd,OVMF.*,*.secboot.*,*Shell*}'
	$buildah_run $buildcntr rpm -e --nodeps qemu-user || true  # Not needed, but is depended upon by qemu-user-binfmt
	$buildah_run $buildcntr dnf5 -y clean all

	# List the largest packages
	$buildah_run $buildcntr sh -c "rpm -qa --queryformat '%10{size} - %-25{name} \t %{version} \t %{os} \n' | sort -rh | head -25 | awk '{print \$1/1024/1024, \$2, \$3, \$4}'"
}
