===========================
Introduction to ci-tron
===========================

This project provides the source for the CI-tron infrastructure, a bare-metal
automated CI system for Linux driver development.

Its main purpose is to build a CI gateway service, with different components,
responsible for the orchestration and management of devices under test, or DUTs.
Each component has been designed with a focus on creating well-designed
interfaces to form an open source toolbox that CI systems can easily
reuse and customize to their requirements, without limiting their future options.

This test system was approached with several design principles in mind:

* Avoiding dependence on specific software, so as to remain flexible and adaptable.

* Implementing auto-discovery to simplify the setup process of DUTs.

* Making the system as easy to use and intuitive as possible, so that users can
  quickly get up and running with it.

* Avoiding complex and difficult-to-manage configuration.

* Ensuring that the system is easy to administer and maintain, so its maintenance
  burden is minimal.


All code is hosted by the `freedesktop.org`_ project at `ci-tron`_.


Bugs and requests
==================

Please use the `GitLab issue tracker`_ to submit bugs or request features.


License
=======

Except where otherwise indicated in a given source file, all original contributions
to this project are licensed under the MIT License.

.. code-block:: text

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.


.. _GitLab issue tracker: https://gitlab.freedesktop.org/gfx-ci/ci-tron/-/issues
.. _ci-tron: https://gitlab.freedesktop.org/gfx-ci/ci-tron/
.. _freedesktop.org: https://www.freedesktop.org/wiki/
